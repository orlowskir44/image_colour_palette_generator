import os
from flask import Flask, render_template, request, flash
from werkzeug.utils import secure_filename
from colorthief import ColorThief


ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}

app = Flask(__name__)
app.config['SECRET_KEY'] = 'SECRETKEY'
app.config['UPLOAD_FOLDER'] = "static/"

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/upload',methods=['POST'])
def upload():
    if 'file' in request.files:
        file = request.files['file']
        filename = secure_filename(file.filename)
        if allowed_file(filename):
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        else:
            flash('Bad file type')
        full_path = f"static/{file.filename}"
        colors = ColorThief(full_path)
        top_colors = colors.get_palette()


        return render_template('upload.html',full_path=full_path, file_name = filename, colors=top_colors)

if __name__ == '__main__':
    app.run(debug=True, port=8000)